CC=m68k-elf-gcc
LD=m68k-elf-ld

CFLAGS=-I. -Os -m68000
LDFLAGS= -nodefaultlibs -nostdlib -static
BUILD_DIR=build

MAME_ROMS_DIR ?= C:/Program Files/mame/roms/
INPUT_ZIP_FILE ?= $(MAME_ROMS_DIR)/ddonpachj.zip.bak
OUTPUT_ZIP_FILE ?= $(MAME_ROMS_DIR)/ddonpachj.zip

patch = echo $(3) | xxd -p -r | dd of=$(1) bs=1 conv=notrunc seek=$(shell echo $$(($(2))) )

.PHONY: install clean disasm
all: $(BUILD_DIR) \
	$(BUILD_DIR)/patch.o \
	$(BUILD_DIR)/ddonpachj.patch.elf \
	$(BUILD_DIR)/ddonpachj.patch.map \
	$(BUILD_DIR)/ddonpachj.patch.hex \
	$(BUILD_DIR)/ddonpachj.patch.ips

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/patch.o: patch.S
	$(CC) -c -o $@ patch.S $(CFLAGS)

$(BUILD_DIR)/ddonpachj.patch.elf $(BUILD_DIR)/ddonpachj.patch.map: ddonpachj.patch.ld $(BUILD_DIR)/patch.o
	$(LD) -T ddonpachj.patch.ld -o $(BUILD_DIR)/ddonpachj.patch.elf $(BUILD_DIR)/patch.o -Map=$(BUILD_DIR)/ddonpachj.patch.map 

$(BUILD_DIR)/ddonpachj.patch.hex: $(BUILD_DIR)/ddonpachj.patch.elf
	m68k-elf-objcopy -O ihex $(BUILD_DIR)/ddonpachj.patch.elf $(BUILD_DIR)/ddonpachj.patch.hex

$(BUILD_DIR)/ddonpachj.patch.ips: $(BUILD_DIR)/ddonpachj.patch.hex
	python patch.py ips $(BUILD_DIR)/ddonpachj.patch.hex $(BUILD_DIR)/ddonpachj.patch.ips

install: $(BUILD_DIR)/ddonpachj.patch.hex
	python patch.py patch "$(BUILD_DIR)/ddonpachj.patch.hex" "$(INPUT_ZIP_FILE)" "$(OUTPUT_ZIP_FILE)"

clean:
	rm -rf $(BUILD_DIR)

disasm: $(BUILD_DIR)/ddonpachj.patch.elf
	m68k-elf-objdump --disassemble-all $(BUILD_DIR)/ddonpachj.patch.elf
