#!/usr/bin/env python3

from intelhex import IntelHex
import zipfile
import sys
import zlib
import argparse
import hashlib


PRESERVE_CRC = False
ORIGINAL_u26_SHA1 = "ae98eba049f1462aa1145f6959b9f9a32c97278f"
ORIGINAL_u27_SHA1 = "fbc826c30553f6553ead40b312b73c049e8f4bf6"


def intelhex_to_ips(ih, ips_path):
    from ips_util import Patch
    patch = Patch()
    for sstart, sstop in ih.segments():
        segment = ih.gets(sstart, sstop-sstart)
        patch.add_record(sstart, segment)

    with open(ips_path, 'wb') as f:
        f.write(patch.encode())


def getroms(zip_path):
    with zipfile.ZipFile(zip_path) as z:
        u26 = z.open('u26.bin').read()
        u27 = z.open('u27.bin').read()
        assert len(u26) == len(u27)
        return u26, u27


def mergeroms(u26, u27):
    assert len(u26) == len(u27)
    rom = bytearray(b"\0\0" * len(u26))
    for i, (b1, b0) in enumerate(zip(u26, u27)):
        rom[i*2:i*2+2] = bytes([b0, b1])
    return bytes(rom)


def splitroms(rom):
    assert len(rom) % 2 == 0
    u26 = bytearray(b"\0" * (len(rom) // 2))
    u27 = bytearray(b"\0" * (len(rom) // 2))
    for i in range(len(u26)):
        u27[i] = rom[i*2]
        u26[i] = rom[i*2+1]
    return u26, u27


def putroms(zip_path, u26, u27):
    with zipfile.ZipFile(zip_path, 'w', compression=zipfile.ZIP_DEFLATED) as z:
        z.open('u26.bin', 'w').write(u26)
        z.open('u27.bin', 'w').write(u27)


def set_crc32(rom, desired_crc):
    from BitVector import BitVector

    crc = zlib.crc32(rom + b"\0\0\0\0") & 0xffffffff
    crc ^= desired_crc
    inverse_crc = BitVector(intVal=crc)

    poly = BitVector(intVal=0x104C11DB7)
    inv = BitVector(intVal=0x100000000).gf_MI(poly, 32)
    k = inverse_crc.reverse()
    p = k.gf_multiply_modular(inv, poly, 32)
    crc_pad = bytes.fromhex(p.reverse().getHexStringFromBitVector())[::-1]
    return rom + crc_pad


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='patch.py', add_help=False)
    subparsers = parser.add_subparsers(dest='command')
    parser1 = subparsers.add_parser('patch')
    parser1.add_argument('patch_hex_path', help='Path of input patch hex')
    parser1.add_argument('input_zip_path', help='Path of input romset zip')
    parser1.add_argument('output_zip_path', help='Path of output romset zip')
    parser2 = subparsers.add_parser('ips')
    parser2.add_argument('patch_hex_path', help='Path of input patch hex')
    parser2.add_argument('output_ips_path', help='Path of output patch ips')
    args = parser.parse_args(sys.argv[1:])
    print(args)

    if args.command == 'patch':
        # Patch a zip file
        patch_hex_path = args.patch_hex_path
        input_zip_path = args.input_zip_path
        output_zip_path = args.output_zip_path

        u26, u27 = getroms(input_zip_path)
        assert hashlib.sha1(u26).hexdigest() == ORIGINAL_u26_SHA1
        assert hashlib.sha1(u27).hexdigest() == ORIGINAL_u27_SHA1
        crc_u26 = zlib.crc32(u26) & 0xffffffff
        crc_u27 = zlib.crc32(u27) & 0xffffffff
        rom = mergeroms(u26, u27)
        ih = IntelHex()
        ih.puts(0, rom)
        ih.merge(IntelHex(patch_hex_path), overlap='replace')
        rom = ih.gets(0, 0x100000)
        u26, u27 = splitroms(rom)

        if PRESERVE_CRC:
            u26 = set_crc32(u26[:-4], crc_u26)
            u27 = set_crc32(u27[:-4], crc_u27)
            assert zlib.crc32(u26) & 0xffffffff == crc_u26
            assert zlib.crc32(u27) & 0xffffffff == crc_u27

        putroms(output_zip_path, u26, u27)

    if args.command == 'ips':
        # Create IPS patch
        patch_hex_path = args.patch_hex_path
        output_ips_path = args.output_ips_path

        ih = IntelHex(patch_hex_path)
        intelhex_to_ips(ih, output_ips_path)
