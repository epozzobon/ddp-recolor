# DoDonPachi Ship Recolor Patch

This rom hack of allows you to change the player ship color during ship selection with the left/right buttons.

It also removes the background music.

Apply it to the `ddonpachj` mame romset

## Instructions

Install the prerequisites:
* m68k-elf-gcc
* python
* [intelhex python module](https://python-intelhex.readthedocs.io/en/latest/)

Build it:
```bash
git clone https://gitlab.com/epozzobon/ddp-recolor
make
````

Install it:
```bash
# Point the MAME_ROMS_DIR variable to your own mame roms directory
export MAME_ROMS_DIR="C:/Program Files/mame/roms"
# Make a backup copy of ddonpachj.zip - make sure you don't overwrite your backup!
cp -i "$MAME_ROMS_DIR/ddonpachj.zip" "$MAME_ROMS_DIR/ddonpachj.zip.bak"
make install
```